# Twitchant Pesterer
Simple twitch.tv chat written in Haskell using wxWidgets and Lua for plugins.

The goal is to create a cross-platform Twitch chat client with native widgets across all platforms.

# Planned Features
* Hitbox.tv support
* Youtube support
* Custom Commands
* Bot features
* Betting
* Point System
* Probably a bunch of other garbage
